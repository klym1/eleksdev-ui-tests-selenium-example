﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Blog.AspNet.Startup))]
namespace Blog.AspNet
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
