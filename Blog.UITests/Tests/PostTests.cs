﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using Blog.UITests.PageObjects;
using Blog.UITests.SeleniumHelpers;
using FluentAssertions;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace Blog.UITests.Tests
{
    [TestFixture]
    public class PostTests
    {
        private IWebDriver _driver;
        private string _baseUrl;

        [OneTimeSetUp]
        public void SetupTest()
        {
            _driver = DriverFactory.Create();
            _baseUrl = "http://localhost:56287/";
            
            Debug.WriteLine("Started");
        }

        [OneTimeTearDown]
        public void TearDownFixture()
        {
            try
            {
                _driver.Quit();
                Debug.WriteLine("Closed");
            }
            catch (Exception ex)
            {
                // Ignore errors if we are unable to close the browser
            }
        }
        
        [Test]
        public void WhenUserCreatesAPost__ItShouldbeStoredInDB_1()
        {
            //Arrange
            _driver.Navigate().GoToUrl(_baseUrl + "/Posts/");

            var existingPostsNumberBeforeTest = _driver.FindElements(By.TagName("tr")).Count;
            _driver.Navigate().GoToUrl(_baseUrl + "/Posts/Create");

            _driver.FindElement(By.Id("Content")).SendKeys("My new fancy content #" + (existingPostsNumberBeforeTest + 1));
            _driver.FindElement(By.Id("Title")).SendKeys("My new fancy title #" + (existingPostsNumberBeforeTest + 1));
            _driver.FindElement(By.Id("CreatedOn")).SendKeys(DateTime.Now.ToString("G", CultureInfo.CurrentCulture));

            var submitButton = _driver.FindElement(By.XPath("//div[@class='form-group']/div/input"));

            //Act
            ClickInput(submitButton);
            Thread.Sleep(1000); // Wait for some time while post is saving in DB

            //Assert
            _driver.Navigate().GoToUrl(_baseUrl + "/Posts/");
            _driver.Navigate().Refresh();
            
            var postsNumberAfterTest = _driver.FindElements(By.TagName("tr")).Count;

            postsNumberAfterTest.Should().Be(existingPostsNumberBeforeTest + 1);
        }

        [Test]
        public void WhenUserCreatesAPost__ItShouldbeStoredInDB_2()
        {
            //Arrange
            _driver.Navigate().GoToUrl(_baseUrl + "/Posts/");

            var existingPostsNumberBeforeTest = _driver.FindElements(By.TagName("tr")).Count;
            _driver.Navigate().GoToUrl(_baseUrl + "/Posts/Create");

            var page = new PostPage();
            PageFactory.InitElements(_driver, page);

            page.ContentValue = "My new fancy content #" + (existingPostsNumberBeforeTest + 1);
            page.TitleValue = "My new fancy title #" + (existingPostsNumberBeforeTest + 1);
            page.CreatedOnValue = DateTime.Now.ToString("G", CultureInfo.CurrentCulture);

            //Act
            page.Submit();
            
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            wait.Until(d => d.Url.EndsWith("/Posts"));
            
            //Assert
            var postsNumberAfterTest = _driver.FindElements(By.TagName("tr")).Count;

            postsNumberAfterTest.Should().Be(existingPostsNumberBeforeTest + 1);
        }

        private void ClickInput(IWebElement elem)
        {
            elem.SendKeys("\n");
        }
    }
}


