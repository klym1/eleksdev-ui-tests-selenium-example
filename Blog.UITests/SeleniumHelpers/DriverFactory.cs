﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Blog.UITests.SeleniumHelpers
{
    public static class DriverFactory
    {
        public static IWebDriver Create()
        {
            var driver = new ChromeDriver();

            driver.Manage().Window.Maximize();
          
            // Suppress the onbeforeunload event first. This prevents the application hanging on a dialog box that does not close.
            ((IJavaScriptExecutor)driver).ExecuteScript("window.onbeforeunload = function(e){};");
            return driver;
        }
    }
}