﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Blog.UITests.PageObjects
{
    public class PostPage
    {
        [FindsBy(How = How.Id, Using = "Content")]
        public IWebElement Content { get; set; }

        [FindsBy(How = How.Id, Using = "Title")]
        public IWebElement Title { get; set; }

        [FindsBy(How = How.Id, Using = "CreatedOn")]
        public IWebElement CreatedOn { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='form-group']/div/input")]
        public IWebElement SubmitButton { get; set; }

        public string ContentValue
        {
            set { Content.SendKeys(value); }
        }

        public string TitleValue
        {
            set { Title.SendKeys(value); }
        }

        public string CreatedOnValue
        {
            set { CreatedOn.SendKeys(value); }
        }

        public void Submit()
        {
            SubmitButton.SendKeys("\n");
        }
    }
}

